require(['jquery', 'Magento_Ui/js/modal/modal'], function($, modal) {
    var options = {
        type: 'popup',
        responsive: true,
        innerScroll: true,
        title: '',
        buttons: [],
        modalClass: 'custom-popup',
        clickableOverlay: true,
        closed: function() {
            // Puedes agregar aquí cualquier acción después de cerrar el popup.
        }
    };

    var popup = modal(options, $('#popup-content'));

    $('#show-popup').on('click', function() {
        $('#popup-content').modal('openModal');
    });
});