define([

    'Magento_Checkout/js/model/quote',
    'Magento_Ui/js/modal/alert',
    'jquery'

], function (quote, Alert,$) {
    'use strict'
    let mixin = {

        initialize: function () {
            this._super();
            self = this;
            quote.shippingAddress.subscribe(function (address) {
                if (address && address.countryId === 'CO') {
                   self.showAlert()
                }
            });
            return this;
        },

        showAlert: function(){
            Alert({
                title: 'Alert Title',
                content: 'Has seleccionado colombia, este es el mejor pais del mundo',
                modalClass: 'alert',
                
                buttons: [{
                    text: $.mage.__('Accept'),
                    class: 'action primary accept',

                    click: function () {
                        this.closeModal(true);
                    }
                }]
            });
        }

    };

    return function (target) {
        return target.extend(mixin);
    };
})