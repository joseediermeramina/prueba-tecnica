function toggleAccordion(element) {
    if (window.innerWidth <= 768) {
        const content = element.nextElementSibling;
        content.style.display = content.style.display === 'block' ? 'none' : 'block';
    }
}